public class Subtract {
    private int number;

    public Subtract(int value) {
        this.number = value;
    }

    public int subtract(int value) {
        return number-value;
    }
}
